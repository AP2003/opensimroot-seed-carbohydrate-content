# OpenSimRoot Seed Carbohydrate Content

This modifies OpenSimRoot to add seed carbohydrate content as a parameter.

## Rationale
Right now, SimRoot uses the `CToDryWeight` factor for the plant to convert seed mass to seed C available for growth, which overestimates seed carbohydrate content according to the literature. Therefore, this code can be used to add two parameters, `seedCarbohydrateToCFactor` and `seedCarbohydrateContent`.

## Support
Feel free to email me at a.perkins.nh@gmail.com

## The details
SimRoot contains multiple functions that can be used to simulate the depletion of seed reserves. This code is for the function commonly used in maize, `reservesSinkBased`. The function is specified in the `Templates` folder (if the input files are in folder format).

In this scenario, seed C is calculated in two functions that need to be modified: `ReservesSinkBased` (in `SeedReserves.cpp`) and `PlantCarbonIncomeRate` (in `CarbonSources.cpp`). What's confusing is that their names in `TabledOutput.tab` (which are specified in `Templates`) and the name of the thing they return differ. Here's a quick summary of that:

```
Function                    C++ File            What it Returns             Name in TabledOut
ReservesSinkBased           SeedReserves.cpp    reservesSinkBased           reserves
PlantCarbonIncomeRate       CarbonSources.cpp   plantCarbonIncomeRate       plantCarbonIncome

```

So, in addition to modifying both those functions and the corresponding header files, we add two new functions to handle the `seedCarbohydrateContent` value and the `seedCarbohydrateToCFactor` in order to follow the structure of the existing code. The new functions are `SeedCtoC` and `SeedCContent` to `Carbon2DryWeight.cpp`. Both have hard coded backward compatibility defaults in the functions, and warnings are returned when the defaults are used. For `SeedCtoC`:

```
if(param){
		param->get(cdw);
	}else{
		cdw=0.444;//backwards compatibility default
		msg::warning("SeedCtoC: seedCarbohydrateToCFactor parameter not found in the resource section of plant "+plantType+". Using default of 0.444.", -1);
	}
}
```
Amylose is (C6H10O5)n, so the total mass is 162.14 per unit, and C is `44.4%` of that. Both amylose and amylopectin are polymers of alpha glucose.

For `SeedCContent`:

```
if(param){
		param->get(cdw);
	}else{
		cdw=0.7331;//backwards compatibility default
		msg::warning("SeedCContent: seedCarbohydrateContent parameter not found in the resource section of plant "+plantType+". Using default of 0.7331 for B73.", -1);
	}
}
```

This value comes from Flint-Garcia et al. 2009.


So, for these functions to be used, some code is needed in `Templates` in a `path="/plantTemplate"` section. I put it in `plantTemplate.IncludeCarbonModule.xml`. Here's that code:

```
		<SimulaDerivative name="seedCarbohydrateToCFactor" unit="100%" function="seedCarbohydrateToCFactor" />
		<SimulaDerivative name="seedCarbohydrateContent" unit="100%" function="seedCarbohydrateContent" />
```

Finally, we need the parameters in `Resources.xml`. Here's that:

```
<SimulaConstant
			name="seedCarbohydrateToCFactor"
			unit="100%">
			<!--Alden 2020 Adding this-->
			<!--Amylose and amylopectin are polymers of alpha glucose, which is
			(C6H10O5)n, so C is 44.4% of that by mass-->
			0.444
		</SimulaConstant>
		<SimulaConstant
			name="seedCarbohydrateContent"
			unit="100%">
			<!--Alden 2020 Adding this-->
			<!--It's as a percentage of kernel weight-->
			<!--B73 is 73.31% carbohydrates by mass (Flint-Garcia et al., 2009)-->
			0.7331
		</SimulaConstant>
```

## Backward Compatibility
Here's a summary of how this will behave in different backward compatibility situations:

```
New Functions Included in Templates?         Paramters Included in Resources?       Outcome
Yes                                          Yes                                    Runs normally
Yes                                          No                                     Uses hard coded defaults for both parameters
No                                           No                                     Uses C to Dry Weight
No                                           Yes                                    Won't run. This is a good behavior. 
```

## Validation
In this directory, there is an HTML file summarizing how the model responds to changes in these parameters.

## Instructions Summarized

The following must be done for this code to work:

1. Compile a version of OpenSimRoot that includes these changes to the `CarbonModule`. You can either compile the `OpenSimRoot` folder that is in here (which might be outdated) or replace the `CarbonModule` folder in your version with the one in this directory (and compile).
2. Include the new parameters in `Resources.xml` (in `PlantParameters/Maize/Maize`). Here are those lines:

```
<SimulaConstant
			name="seedCarbohydrateToCFactor"
			unit="100%">
			<!--Alden 2020 Adding this-->
			<!--Amylose and amylopectin are polymers of alpha glucose, which is
			(C6H10O5)n, so C is 44.4% of that by mass-->
			0.444
		</SimulaConstant>
		<SimulaConstant
			name="seedCarbohydrateContent"
			unit="100%">
			<!--Alden 2020 Adding this-->
			<!--It's as a percentage of kernel weight-->
			<!--B73 is 73.31% carbohydrates by mass (Flint-Garcia et al., 2009)-->
			0.7331
		</SimulaConstant>
```

3. Include the new functions in `Templates` by adding the lines below to the `path="/plantTemplate"` section of `plantTemplate.IncludeCarbonModule.xml`:

```
		<SimulaDerivative name="seedCarbohydrateToCFactor" unit="100%" function="seedCarbohydrateToCFactor" />
		<SimulaDerivative name="seedCarbohydrateContent" unit="100%" function="seedCarbohydrateContent" />
```

## What's in this directory

1. Sample `InputFiles`
2. The `CarbonModule` folder, which could be put in the current version of OSR for compilation (Under `scr/modules`). The specific files that need to be changed for this to work are `Carbon2DryWeight.cpp`, `Carbon2DryWeight.hpp`, `CarbonSources.cpp`, `CarbonSources.hpp`, `SeedReserves.cpp`, and `SeedReserves.hpp`.
3. The modified version of `OpenSimRoot` that was used for the manuscript. It might be outdated.
